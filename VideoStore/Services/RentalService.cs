﻿using System.Collections.Generic;
using System.Linq;
using VideoStore.Models;

namespace VideoStore.Services
{
    public class RentalService
    {
        private List<Rental> _Rentals;

        public RentalService()
        {
            _Rentals = new List<Rental>();
        }

        public List<Rental> Rentals { get => _Rentals;}

        public Rental Rent(Movie movie, int daysRented, Customer customer)
        {
            var rental = new Rental(movie,  daysRented,  customer);
            _Rentals.Add(rental);

            return rental;
        }

        public Quote GetQuoteForCustomer(Customer customer)
        {
            var customersRentals = _Rentals.Where(c => c.Customer.Id == customer.Id);

            return GetQuote(customersRentals);
        }

        public Quote GetQuote(Rental rental)
        {
            var quote = new Quote();

            switch (rental.Movie.PriceCode)
            {
                case MovieType.REGULAR:
                    if (rental.DaysRented > 2)
                        quote.Cost = (rental.DaysRented - 2) * 1.5;
                    else quote.Cost = 2;
                    break;
                case MovieType.NEW_RELEASE:
                    quote.Cost = rental.DaysRented * 3;
                    break;
                case MovieType.CHILDRENS:
                    if (rental.DaysRented > 3)
                        quote.Cost = (rental.DaysRented - 3) * 1.5;
                    else
                        quote.Cost = 1.5;
                    break;
            }

            quote.FrequentRenterPoints = 1;

            if (rental.Movie.PriceCode == MovieType.NEW_RELEASE
                    && rental.DaysRented > 1)
                quote.FrequentRenterPoints = 2;

            return quote;
        }

        public Quote GetQuote(IEnumerable<Rental> rentals)
        {
           var allQuotes = rentals.Select(rental => GetQuote(rental));

            var quote = new Quote();
            quote.Cost = allQuotes.Select(c => c.Cost).Sum();
            quote.FrequentRenterPoints = allQuotes.Select(c => c.FrequentRenterPoints).Sum();

            return quote;
        }

    }
}
