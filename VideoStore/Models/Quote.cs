﻿
namespace VideoStore.Models
{
    public class Quote
    {
        /// <summary>
        /// Cost of renting
        /// </summary>
        public double Cost { get; set; }

        /// <summary>
        /// Points earned by customer by renting
        /// </summary>
        public int FrequentRenterPoints { get; set; }
    }
}
