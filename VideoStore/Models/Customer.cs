﻿using System;
using System.Collections.Generic;

namespace VideoStore.Models
{
    public class Customer
    {
		private Guid _Id;
		private string _Name;
		private List<Rental> rentals;

		public Customer(string name)
		{
			_Id = new Guid();
			this._Name = name;
		}

		public string Name { get => _Name; set => _Name = value; }
		public Guid Id { get => _Id;  }
	}
}
