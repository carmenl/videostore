﻿
namespace VideoStore.Models
{
	public class Movie
	{
		private string title;
		private int priceCode;

		public Movie(string title, int priceCode)
		{
			this.title = title;
			this.priceCode = priceCode;
		}

		public string Title
		{
			get { return title; }
		}

		public int PriceCode
		{
			get { return priceCode; }
			set { priceCode = value; }
		}
	}
}
