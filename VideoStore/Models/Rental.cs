﻿using System;

namespace VideoStore.Models
{
    public class Rental
    {
		private Guid RentalId;
		private Movie _Movie;
		private int _DaysRented;
		private Customer _Customer;

		public Rental(Movie movie, int daysRented, Customer customer)
		{
			this.RentalId = new Guid();
			this.Movie = movie;
			this._DaysRented = daysRented;
			this._Customer = customer;
		}

		public Movie Movie { get => _Movie; set => _Movie = value; }
		public int DaysRented { get => _DaysRented; set => _DaysRented = value; }
		public Customer Customer { get => _Customer; set => _Customer = value; }
	}
}
