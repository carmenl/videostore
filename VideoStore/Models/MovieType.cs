﻿
namespace VideoStore.Models
{
    public static class MovieType
    {
        public const int REGULAR =         0;
        public const int NEW_RELEASE =     1;
        public const int CHILDRENS =       2;

    }
}
