﻿using NUnit.Framework;
using VideoStore.Models;
using VideoStore.Services;

namespace VideoStore.Tests
{

	[TestFixture]
	public class CustomerTests
	{
		private Customer customer;
		private RentalService service;

		[SetUp]
		public void Setup()
		{
			customer = new Customer("Fred");
			service = new RentalService();
		}

		[Test]
		public void SingleNewReleaseQuote()
		{
			var rental = service.Rent(new Movie("The Cell", MovieType.NEW_RELEASE), 3, customer);

			var quote = service.GetQuote(rental);

			Assert.AreEqual(9d, quote.Cost);
			Assert.AreEqual(2, quote.FrequentRenterPoints);

			var quoteForCustomer = service.GetQuoteForCustomer(customer);

			Assert.AreEqual(9d, quoteForCustomer.Cost);
			Assert.AreEqual(2, quoteForCustomer.FrequentRenterPoints);
		}

		[Test]
		public void DualNewReleaseQuote()
		{

			service.Rent(new Movie("The Cell", MovieType.NEW_RELEASE), 3, customer);
			service.Rent(new Movie("The Tigger Movie", MovieType.NEW_RELEASE), 3, customer);

			var quote = service.GetQuoteForCustomer(customer);

			Assert.AreEqual(18d, quote.Cost);
			Assert.AreEqual(4, quote.FrequentRenterPoints);
		}

		[Test]
		public void SingleChildrensQuote()
		{
			var rental = service.Rent(new Movie("The Tigger Movie", MovieType.CHILDRENS), 3, customer);

			var quote = service.GetQuote(rental);

			Assert.AreEqual(1.5d, quote.Cost);
			Assert.AreEqual(1, quote.FrequentRenterPoints);

		}

		[Test]
		public void MultipleRegularQuote()
		{

			service.Rent(new Movie("Plan 9 from Outer Space", MovieType.REGULAR), 1, customer);
			service.Rent(new Movie("8 1/2", MovieType.REGULAR), 2, customer);
			service.Rent(new Movie("Eraserhead", MovieType.REGULAR), 3, customer);

			var quote = service.GetQuoteForCustomer(customer);

			Assert.AreNotEqual(7.5d, quote.Cost);
			Assert.AreEqual(5.5d, quote.Cost);
			Assert.AreEqual(3, quote.FrequentRenterPoints);

		}

	}
}
